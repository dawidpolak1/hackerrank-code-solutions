package datastructures.java1darraypart2;

public class RecursionExample {

    static int sum(int n) {
        if (n > 1) {
            //System.out.println(n + " +" + "sum(" + (n - 1) + ")");
            return n + sum(n - 1);
        } else {
            return 1;
        }
    }

    public static void main(String[] args) {

        int result = sum(3);
        System.out.println(result);


    }
}
