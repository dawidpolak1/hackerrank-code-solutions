package solvingproblems.easy.fifteenpoints.thehurdlerace;

import java.util.Arrays;

public class Solution {

    private static int countMinNoOfDosesRequired(int[] height, int maxHeightOfJump) {
                Arrays.sort(height);
                int hurdleMaxHeight = height[height.length - 1];
                if (maxHeightOfJump > hurdleMaxHeight)
                    return 0;
                else  return hurdleMaxHeight - maxHeightOfJump;
    }

    public static void main(String[] args) {

        int[] height = new int[] {1, 2, 3, 3, 2};
        int maxHeightOfJump = 1;
        int result = countMinNoOfDosesRequired(height, maxHeightOfJump);
        System.out.println(result);
    }
}
