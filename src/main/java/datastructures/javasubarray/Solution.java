package datastructures.javasubarray;

public class Solution {

    static void printNumberOfNegativeSum(int[] numbers) {
        int n = numbers.length;
        int negativeSumsCount = 0;

        for (int i = 0;  i < n; i++) {
            int runningSum = 0;

            for (int j = i; j < n; j++) {
                runningSum += numbers[j];

                if (runningSum < 0) {
                    negativeSumsCount++;
                }
            }
        }
        System.out.println(negativeSumsCount);
    }

    public static void main(String[] args) {

        int[] numbers = new int[] {1, -2, 4, -5, 1};
        printNumberOfNegativeSum(numbers);
    }
}
