package objectorientedprogramming.javaiterator;

import java.util.*;

public class Solution {

    private static final String TRIPLE_HASH = "###";

    static ArrayList<Object> func(ArrayList<Object> myList){
        myList.removeIf(element -> element instanceof Integer || element.equals(TRIPLE_HASH));
        return myList;
    }

    public static void main(String[] args) {

        ArrayList<Object> myList = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();

        for(int i = 0; i<n; i++){
            myList.add(sc.nextInt());
        }

        myList.add(TRIPLE_HASH);
        for(int i=0; i<m; i++){
            myList.add(sc.next());
        }

        ArrayList<Object> listWithStrings = func(myList);
        for (Object element : listWithStrings) {
            System.out.println(element);
        }
    }
}
