package objectorientedprogramming.javainheritance2;

public class Solution {

   static class Arithmetic {

        static int add(int a, int b) {
            return a + b;
        }
    }

    static class Adder extends Arithmetic {

    }

    public static void main(String[] args) {

        System.out.println("My superclass is: " + Arithmetic.class.getSimpleName());
        System.out.print(Adder.add(21, 21) + " " + Adder.add(6, 7) + " " + Adder.add(18, 2));
    }
}
