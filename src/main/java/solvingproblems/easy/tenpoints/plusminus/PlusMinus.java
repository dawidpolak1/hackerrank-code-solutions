package solvingproblems.easy.tenpoints.plusminus;

class PlusMinus {

    private static final int ZERO = 0;
    private double positiveCount = 0;
    private double negativeCount = 0;
    private double zeroCount = 0;

    public double getPositiveCount() {
        return positiveCount;
    }

    public double getNegativeCount() {
        return negativeCount;
    }

    public double getZeroCount() {
        return zeroCount;
    }

    void countAndPrintRatios(int[] arr) {
        countRatios(arr);
        printRatios(positiveCount, negativeCount, zeroCount, arr);
    }

    private void countRatios(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > ZERO) {
                positiveCount++;
            } else if (arr[i] < ZERO) {
                negativeCount++;
            } else if (arr[i] == ZERO) {
                zeroCount++;
            }
        }
    }

    private static void printRatios(double positiveCount, double negativeCount, double zeroCount, int[] arr) {
        System.out.printf("%.6f\n", positiveCount / arr.length);
        System.out.printf("%.6f\n", negativeCount / arr.length);
        System.out.printf("%.6f\n", zeroCount / arr.length);
    }
}
