package datastructures.java2darray;

public class Solution {

    private static int findSumOfHourglass(int[][] array2D, int row, int col) {
        int top = array2D[row][col] + array2D[row][col + 1] + array2D[row][col + 2];
        int center = array2D[row + 1][col + 1];
        int bottom = array2D[row + 2][col] + array2D[row + 2][col + 1] + array2D[row + 2][col + 2];
        return top + center + bottom;
    }

    private static void findMaxHourglass(int[][] array2D) {
        int maxSum = Integer.MIN_VALUE;

        for (int row = 0; row < 4; row++) {
            for (int col = 0; col < 4; col++) {
                int sum = findSumOfHourglass(array2D, row, col);
                maxSum = Math.max(maxSum, sum);
            }
        }
        System.out.println("Hourglass with largest sum: " + maxSum);
    }

    public static void main(String[] args) {

        /**
         * Converting String to 1D array
         *
         * String strings = "1 1 1 0 0 0 0 1 0 0 0 0 1 1 1 0 0 0 0 0 2 4 4 0 0 0 0 2 0 0 0 0 1 2 4 0";
         *
         *         String[] split = strings.split(" ");
         *         int[] array = new int[split.length];
         *
         *         for (int i = 0; i < split.length; i++) {
         *             array[i] = Integer.parseInt(split[i]);
         *         }
         */

        /**
         * Converting 1D array to 2D array
         *
         * int[][] array2d = new int[6][6];
         *         int ROWS = 6;
         *         int COLUMNS = 6;
         *         int arrayStartIndex = 0;
         *
         *         for (int i = 0; i < ROWS; i++) {
         *
         *             for (int j = 0; j < COLUMNS; j++) {
         *                 array2d[i][j] = array[arrayStartIndex];
         *                 arrayStartIndex++;
         *             }
         *         }
         *
         *         System.out.println(Arrays.deepToString(array2d));
         */

        int[][] array2D = new int[][]
                {
                        {1, 1, 1, 0, 0, 0},
                        {0, 1, 0, 0, 0, 0},
                        {1, 1, 1, 0, 0, 0},
                        {0, 0, 2, 4, 4, 0},
                        {0, 0, 0, 2, 0, 0},
                        {0, 0, 1, 2, 4, 0}
                };

//        for (int row = 0; row < 6; row++) {
//            for (int column = 0; column < 6; column++)
//
//        }
        findMaxHourglass(array2D);
    }
}
