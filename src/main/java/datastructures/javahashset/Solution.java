package datastructures.javahashset;

import java.util.*;

public class Solution {

    public static void main(String[] args) {

       Scanner in = new Scanner(System.in);
       Set<String> stringSet = new HashSet<>();

       int lines = in.nextInt();
       int counter = 0;
       for (int i = 0; i < lines; i++) {
           String uniquePairs = in.next() + " " + in.next();
           if (!stringSet.contains(uniquePairs)) {
               counter++;
           }
           stringSet.add(uniquePairs);
           System.out.println(counter);
       }
    }
}
