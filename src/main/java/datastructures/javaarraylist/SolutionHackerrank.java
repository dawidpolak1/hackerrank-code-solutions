package datastructures.javaarraylist;

import java.util.ArrayList;
import java.util.Scanner;

public class SolutionHackerrank {

    private static ArrayList<ArrayList<Integer>> create2DArrayList(Scanner sc) {
        int lines = sc.nextInt();
        ArrayList<ArrayList<Integer>> array2DList = new ArrayList<>();

        for (int row = 0; row < lines; row++) {
            int numOfInts = sc.nextInt();
            ArrayList<Integer> list = new ArrayList<>();
            for (int column = 0; column < numOfInts; column++) {
                list.add(sc.nextInt());
            }
            array2DList.add(list);
        }
        return array2DList;
    }

    private static void printNumberOnGivenPosition(Scanner sc,
                   ArrayList<ArrayList<Integer>> array2DList) {

        int numOfQueries = sc.nextInt();
        for (int i = 0; i < numOfQueries; i++) {
            int line = sc.nextInt();
            int position = sc.nextInt();

            ArrayList<Integer> list = array2DList.get(line - 1);
            if (position <= list.size()) {
                System.out.println(list.get(position - 1));
            } else {
                System.out.println("ERROR!");
            }
        }
    }

    public static void main(String[] args) {

        try (Scanner sc = new Scanner(System.in)) {
            ArrayList<ArrayList<Integer>> array2DList = create2DArrayList(sc);
            printNumberOnGivenPosition(sc, array2DList);
        }
    }
}
