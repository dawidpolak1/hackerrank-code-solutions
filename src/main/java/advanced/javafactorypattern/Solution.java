package advanced.javafactorypattern;

import java.util.Scanner;

interface Food {
    String getType();
}

class Cake implements Food {

    @Override
    public String getType() {
        return "Someone ordered a Dessert!";
    }
}

class Pizza implements Food {

    @Override
    public String getType() {
        return "Someone ordered a Fast Food!";
    }
}

class FoodFactory {

    private static final String PIZZA = "pizza";
    private static final String CAKE = "cake";

    Food getFood(String order) {
        if (order.equalsIgnoreCase(PIZZA))
            return new Pizza();
        if (order.equalsIgnoreCase(CAKE))
            return new Cake();
        return null;
    }
}

public class Solution {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        FoodFactory foodFactory = new FoodFactory();
        Food food = foodFactory.getFood(sc.nextLine());
        sc.close();

        System.out.println("The factory returned " + food.getClass());
        System.out.println(food.getType());
    }
}
