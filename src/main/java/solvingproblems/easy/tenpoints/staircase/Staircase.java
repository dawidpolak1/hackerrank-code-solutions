package solvingproblems.easy.tenpoints.staircase;

public class Staircase {

    /**
     * Using replace method from String
     */
    private static void myStairCase(int n) {
        for (int i = 0; i < n; i++) {
            String appendStar = new String(new char[i]).replace("\0", "#");
            String star = "#" + appendStar;
            System.out.format("%" + n + "s%n", star);
        }
    }

    private static void stairCase(int n) {
        int spaceCount = n;
        int hashCount = n - (n - 1);
        for (int i = 0; i < n; i++) {
            spaceCount--;
            hashCount++;

            for (int j = 1; j <= spaceCount; j++) {
                System.out.print(" ");
            }
            for (int k = 1; k < hashCount; k++) {
                System.out.print("#");
            }
            System.out.println();
        }
    }

    private static void betterStairCase(int n) {
        String rightAligned = "";

        for (int i = 0; i < n; i++) {
            rightAligned += "#";
            System.out.format("%" + n + "s%n", rightAligned);
        }
    }


    public static void main(String[] args) {

        int n = 6;
        myStairCase(n);
    }
}


