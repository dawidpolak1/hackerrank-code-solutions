package introduction.javadatatypes;

public class Solution {

    private static void printFittedDataTypes(long[] numbers) {

        for (long number : numbers) {
            boolean isByte = number >= -128 && number <= 127;
            boolean isShort = number >= -32768 && number <= 32767;
            boolean isInt = number >= -Math.pow(2, 31) && number <= Math.pow(2, 31) - 1;
            boolean isLong = number >= -Math.pow(2, 63) && number <= Math.pow(2, 63) - 1;

            try {
                System.out.println(number + " can be fitted in:");
                if (isByte) System.out.println("* byte");
                if (isShort) System.out.println("* short");
                if (isInt) System.out.println("* int");
                if (isLong) System.out.println("* long");
            } catch (Exception e) {
                System.out.println(number + " can't be fitted anywhere.");
            }
        }
    }

    public static void main(String[] args) {

            long[] numbers = new long[] {-150, -150000, 1500000000};
            printFittedDataTypes(numbers);

    }
}
