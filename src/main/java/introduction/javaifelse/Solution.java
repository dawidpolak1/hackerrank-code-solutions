package introduction.javaifelse;

class Solution {

    private static void printWeirdOrNot (int N) {
        boolean isEven = N % 2 == 0;
        boolean isOdd = N % 2 != 0;

        if (isOdd)
            System.out.print("Weird");
        if (isEven && N >= 2 && N <= 5)
            System.out.print("Not Weird");
        if (isEven && N >= 6 && N <= 20)
            System.out.print("Weird");
       if (isEven && N > 20)
            System.out.println("Not Weird");
    }

    public static void main(String[] args) {

        int N = 24;
        printWeirdOrNot(N);
    }
}
