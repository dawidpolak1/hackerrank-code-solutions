package strings.javasubstring;

public class Solution {

    public static void main(String[] args) {

        String S = "Helloworld";
        int start = 3;
        int end = 7;
        String result = S.substring(start, end);
        System.out.println(result);
    }
}