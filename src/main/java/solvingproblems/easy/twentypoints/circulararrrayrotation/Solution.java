package solvingproblems.easy.twentypoints.circulararrrayrotation;

import java.util.Arrays;

public class Solution {

    static int[] circularArrayRotation(int[] a, int k, int[] queries) {
        int[] rightRotated = circularArrayRotation(a, k);
        return find(rightRotated, queries);
    }

    static int[] circularArrayRotation(int[] a, int k) {
        // Rotate the given array by k times toward right
        for (int i = 0; i < k; i++) {
            int j;
            int lastElement;

            // stores the last element of array in lastElement
            lastElement = a[a.length - 1];

            for (j = a.length - 1; j > 0; j--) {
                a[j] = a[j - 1];
            }

            // last element of an array will be added to the start index of array
            a[0] = lastElement;
        }

        return a;
    }

    private static int[] find(int[] a, int[] queries) {
        for(int i=0 ; i<queries.length ; i++)
            queries[i] = a[queries[i]];
        return queries;
    }

    public static void main(String[] args) {

        int[] a = new int[] {1, 2, 3};
        int k = 2;
        int[] queries = new int[] {2, 1};

        System.out.println("Original array:\n" + Arrays.toString(a));
        circularArrayRotation(a, k);
        System.out.println("\nArray after rotation by " + k + " times:\n" + Arrays.toString(a));
    }
}
