package javaexceptionhandling.trycatch;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {

        try (Scanner in = new Scanner(System.in)) {
            int x = in.nextInt();
            int y = in.nextInt();
            System.out.println(x/y);

        } catch (InputMismatchException e) {
            System.out.println(e.getClass().getCanonicalName());
        } catch (ArithmeticException e) {
            System.out.println(e.getClass().getCanonicalName() + ": / by zero");
        }
    }
}
