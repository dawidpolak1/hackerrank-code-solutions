package solvingproblems.easy.tenpoints.timeconversion;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class Solution {

    static void timeConversion(String s) {

        LocalTime localTime = LocalTime.parse(
                s, DateTimeFormatter.ofPattern("hh:mm:ssa"));

        String formattedTime = localTime.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
        System.out.println(formattedTime);
    }

    public static void main(String[] args) {

        String s = "12:00:01PM"; // Return '12:01:00'
        String s2 = "12:01:00AM"; // 00:01:00

        timeConversion(s);
        timeConversion(s2);
    }
}
