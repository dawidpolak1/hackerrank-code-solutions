package datastructures.java1darraypart2;

import java.util.Scanner;
import java.util.Stack;

public class Solution {

    static boolean canWin(int[] game, int jump) {
        int gameLength = game.length;
        Stack<Integer> integerStack = new Stack<>();

        int start = 0;
        integerStack.push(start);
        while (!integerStack.isEmpty()) {
            int index = integerStack.pop();

            if (index >= gameLength) return true;
            if (index < 0 || game[index] == 1) continue;

            // marks index as visited
            game[index] = 1;

            move(jump, integerStack, index);
        }
        return false;
    }

    private static void move(int jump, Stack<Integer> integerStack, int index) {
        integerStack.push(index + jump);
        integerStack.push(index + 1);
        integerStack.push(index - 1);
    }

    public static void main(String[] args) {

        Scanner in= new Scanner(System.in);
        int queries = in.nextInt();

        while(queries-- > 0) {
            int gameLength = in.nextInt();
            int jump = in.nextInt();

            int[] game = new int[gameLength];
            for (int i = 0; i < gameLength; i++) {
                game[i] = in.nextInt();
            }
            String result = canWin(game, jump) ? "YES" : "NO";
            System.out.println(result);
        }

        in.close();
    }
}
