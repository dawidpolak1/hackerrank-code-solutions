package bignumber.javaprimalitytest;

import java.math.BigInteger;
import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        BigInteger bigNumber = in.nextBigInteger();
        in.close();

        boolean isPrimeOrNot = bigNumber.isProbablePrime(1);

        if (isPrimeOrNot) {
            System.out.println("prime");
        } else {
            System.out.println("not prime");
        }
    }
}