package introduction.javaloopsI;

public class Solution {

    private static void printFirstTenMultiples(int N) {
        int multiples = 0;

        for (int i = 0; i < 10; i++) {
            multiples++;
            int result = N * multiples;


            System.out.println(N + " x " + multiples + " = " + result);
        }
    }

    public static void main(String[] args) {

        int N = 2;
        printFirstTenMultiples(N);
    }
}
