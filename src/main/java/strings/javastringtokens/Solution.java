package strings.javastringtokens;

import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {

        try (Scanner sc = new Scanner(System.in)) {
            if (!sc.hasNext()) {
                System.out.print(0); return;
            }
            String sentence = sc.nextLine();
            String[] splitSentence = sentence
                    .trim()
                    .split("[ !,?._'@]+");

            System.out.println(splitSentence.length);

            for (String token : splitSentence) {
                System.out.println(token);
            }
        }
    }
}
