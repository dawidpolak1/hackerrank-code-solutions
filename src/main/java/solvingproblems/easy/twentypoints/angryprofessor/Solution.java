package solvingproblems.easy.twentypoints.angryprofessor;

public class Solution {

    private static final String CANCELLED = "YES";
    private static final String NOT_CANCELLED = "NO";


    static String angryProfessorAlternative(int thresholdNoOfStudents, int[] arr) {
        int attendance = 0;

        for (int student : arr) {
            if (student > 0) continue;
            else if (student <= 0) {
                attendance++;
            }
        }
        return attendance >= thresholdNoOfStudents ? NOT_CANCELLED : CANCELLED;
    }



    public static void main(String[] args) {

        int thresholdNoOfStudents = 4;
//        int[] arr = {-2, -1, 0, 1, 2};
//        int[] arr = {-1, -3, 4, 2};
//        int[] arr = {0, -1, 2, 1};
        int[] arr = {-93, -86, 49, -62, -90, -63, 40, 72, 11, 67};
        int n = arr.length;

        String result = angryProfessorAlternative(thresholdNoOfStudents, arr);
        System.out.println(result);

    }
}
