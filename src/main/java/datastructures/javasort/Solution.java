package datastructures.javasort;

import java.util.Comparator;
import java.util.LinkedList;

public class Solution {

    public static void main(String[] args) {

        LinkedList<Student> students = new LinkedList<>();
        students.add(new Student(33, "Rumpa", 3.68));
        students.add(new Student(85, "Ashis", 3.85));
        students.add(new Student(56, "Samiha", 3.75));
        students.add(new Student(19, "Samara", 3.75));
        students.add(new Student(22, "Fahim", 3.76));
        students.add(new Student(74, "Samiha", 3.75));

        students.sort(new CustomStudentComparator());

        for (Student student : students) {
            System.out.println(student.getName());
        }
    }

    static class Student {

        int id;
        String name;
        double score;

        Student(int id, String name, double score) {
            this.id = id;
            this.name = name;
            this.score = score;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public double getScore() {
            return score;
        }

        @Override
        public String toString() {
            return "Student{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", score=" + score +
                    '}';
        }
    }

    static class CustomStudentComparator implements Comparator<Student> {

        @Override
        public int compare(Student s1, Student s2) {
            int scoreCompare = Double.compare(s1.getScore(), s2.getScore());
            int nameCompare = s1.getName().compareTo(s2.getName());
            int idCompare = Integer.compare(s1.getId(), s2.getId());

            if (scoreCompare == 0) {
                return nameCompare;
            } else if (s1.getName().equals(s2.getName())) {
                return idCompare;
            }
            return -scoreCompare;
        }
    }
}