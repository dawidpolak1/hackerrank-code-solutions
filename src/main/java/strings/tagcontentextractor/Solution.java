package strings.tagcontentextractor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {

    public static void main(String[] args) {

        String lineOfText1 = "<h1><h1>Sanjay has no watch</h1></h1><par>So wait for a while</par>";
        String lineOfText2 = "<h1>Nayeem loves counseling</h1>";
        String lineOfTex3 = "<Amee>safat codes like a ninja</amee>";

        boolean matchIsFound = false;
        Pattern pattern = Pattern.compile("<(.+)>([^<]+)</\\1");
        Matcher matcher = pattern.matcher(lineOfTex3);

        while(matcher.find()) {
            System.out.println(matcher.group(2));
            matchIsFound = true;
        }

        if (! matchIsFound) {
            System.out.println("None");
        }
    }
}
