package advanced.javavarargs;

import java.io.*;

public class Solution {

    static class Add {

        static void add(int... numbers) {
            StringBuilder sb = new StringBuilder();
            int sum = 0;
            for (int number : numbers) {
                sb.append(number).append("+");
                sum += number;
            }
            sb.setCharAt(sb.length() - 1, '=');
            sb.append(sum);
            System.out.println(sb);

        }
    }

    public static void main(String[] args) {

        try (BufferedReader in = new BufferedReader(new InputStreamReader(System.in))) {
            int n1 = Integer.parseInt(in.readLine());
            int n2 = Integer.parseInt(in.readLine());
            int n3 = Integer.parseInt(in.readLine());
            int n4 = Integer.parseInt(in.readLine());
            int n5 = Integer.parseInt(in.readLine());
            int n6 = Integer.parseInt(in.readLine());

            Add.add(n1, n2);
            Add.add(n1, n2, n3);
            Add.add(n1, n2, n3, n4, n5);
            Add.add(n1, n2, n3, n4, n5, n6);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
