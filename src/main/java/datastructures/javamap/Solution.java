package datastructures.javamap;

import java.io.*;
import java.util.*;

public class Solution {

    private static void enterNameAndFindNumber(Map<String, String> phoneBook, BufferedReader in) throws IOException {
        String queryWithName;
        while((queryWithName = in.readLine()) != null) {
            if (phoneBook.containsKey(queryWithName)) {
                System.out.println(queryWithName + "=" + phoneBook.get(queryWithName));
            }
            else {
                System.out.println("Not found");
            }
        }
    }

    private static void askForNameAndNumber(Map<String, String> phoneBook, BufferedReader in, int entries) throws IOException {
        for (int i = 0; i < entries; i++) {
            System.out.println("Enter name:");
            String name = in.readLine();
            System.out.println("Enter phone number:");
            String number = in.readLine();
            phoneBook.put(name, number);
        }
    }

    public static void main(String[] args) {

        Map<String, String> phoneBook = new HashMap<>();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println("Enter number of entries:");
            int entries = Integer.parseInt(in.readLine());

            askForNameAndNumber(phoneBook, in, entries);
            enterNameAndFindNumber(phoneBook, in);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
