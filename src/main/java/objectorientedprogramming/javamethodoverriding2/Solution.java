package objectorientedprogramming.javamethodoverriding2;

public class Solution {

    static class Bicycle {

        String getDescription() {
            return "a vehicle with pedals.";
        }
    }

    static class Motorcycle extends Bicycle {

        @Override
        String getDescription() {
            return "a cycle with an engine.";
        }
    }

    public static void main(String[] args) {

        Motorcycle motorcycle = new Motorcycle();
        System.out.println("Hello I am a motorcycle, I am " + motorcycle.getDescription());
        Bicycle bicycle = new Bicycle();
        System.out.println("My ancestor is a cycle who is " + bicycle.getDescription());
    }
}
