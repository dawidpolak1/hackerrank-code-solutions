package datastructures.javaarraylist;

import java.util.ArrayList;
import java.util.Arrays;

public class Solution {

    static ArrayList<ArrayList<Integer>> create2DArrayList() {
        ArrayList<ArrayList<Integer>> numberLists = new ArrayList<>();

        numberLists.add(new ArrayList<>(Arrays.asList(41, 77, 74, 22, 44)));
        numberLists.add(new ArrayList<>(Arrays.asList(12)));
        numberLists.add(new ArrayList<>(Arrays.asList(37, 34, 36, 52)));
        numberLists.add(new ArrayList<>(Arrays.asList()));
        numberLists.add(new ArrayList<>(Arrays.asList(20, 22, 33)));

        return numberLists;
    }

    static ArrayList<ArrayList<Integer>> create2DQueryList() {
        ArrayList<ArrayList<Integer>> queryList = new ArrayList<>();

        queryList.add(new ArrayList<>(Arrays.asList(1, 3)));
        queryList.add(new ArrayList<>(Arrays.asList(3, 4)));
        queryList.add(new ArrayList<>(Arrays.asList(3, 1)));
        queryList.add(new ArrayList<>(Arrays.asList(4, 3)));
        queryList.add(new ArrayList<>(Arrays.asList(5, 5)));

        return queryList;
    }

    public static void main(String[] args) {

        ArrayList<ArrayList<Integer>> arrayList = create2DArrayList();
        ArrayList<ArrayList<Integer>> queryList = create2DQueryList();

        for (int i = 0; i < queryList.size(); i++) {
            Integer line = queryList.get(i).get(0);
            Integer position = queryList.get(i).get(1);
            ArrayList<Integer> integers = arrayList.get(line - 1);

            if (position <= integers.size()) {
                System.out.println(integers.get(position - 1));
            } else {
                System.out.println("ERROR!");
            }
        }
    }
}
