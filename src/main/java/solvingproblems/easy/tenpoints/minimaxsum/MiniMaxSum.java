package solvingproblems.easy.tenpoints.minimaxsum;

import java.util.Arrays;

public class MiniMaxSum {

    static void miniMaxSum(int[] arr) {
        Arrays.sort(arr);
        long minResult = 0;
        long maxResult = 0;
        for (int number : arr) {
            minResult += number;
            maxResult += number;
        }
        countAndPrintResults(arr, minResult, maxResult);
    }

    private static void countAndPrintResults(int[] arr, long minResult, long maxResult) {
        int minValue = arr[0];
        int maxValue = arr[arr.length - 1];
        minResult -= maxValue;
        maxResult -= minValue;
        System.out.println(minResult + " " + maxResult);
    }

    public static void main(String[] args) {

        int[] arr = new int[]{1, 3, 5, 7, 9};
        miniMaxSum(arr);
    }
}
