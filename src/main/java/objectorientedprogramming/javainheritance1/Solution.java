package objectorientedprogramming.javainheritance1;

public class Solution {

    static class Animal {

        static void walk(){
            System.out.println("I am walking");
        }
    }

    static class Bird extends Animal {

        static void sing() {
            System.out.println("I am singing");
        }

        static void fly() {
            System.out.println("I am flying");
        }
    }

    public static void main(String[] args) {

        Bird.walk();
        Bird.fly();
        Bird.sing();
    }
}
