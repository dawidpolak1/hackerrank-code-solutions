package introduction.javaloopsII;

public class Solution {

    private static void printIntegers(int a, int b, int n) {
        int powerCount = 0;
        int result = 0;

        for (int i = 0; i < n; i++, powerCount++) {
            int powerOfTwo = (int) Math.pow(2, powerCount);
            int nextElement = a + powerOfTwo * b;
            result += nextElement;
            System.out.print(result + " ");
        }
    }

    public static void main(String[] args) {

        int a = 0;
        int b = 2;
        int n = 10;

        printIntegers(a, b, n);
    }
}
