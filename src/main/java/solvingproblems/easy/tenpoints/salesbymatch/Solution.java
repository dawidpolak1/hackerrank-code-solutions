package solvingproblems.easy.tenpoints.salesbymatch;

import java.util.Arrays;

public class Solution {

    static int sockMerchant(int[] ar, int n) {
        Arrays.sort(ar);
        int pairsOfSocks = 0;

        int i = 0;
        while (i < n) {
            // take first firstNumber
            int firstNumber = ar[i];
            int count = 1;
            i++;

            // count all duplicates
            while (i < n && ar[i] == firstNumber) {
                count++;
                i++;
            }

            // if we spotted firstNumber just two times, then increment result
            if (count >= 2) {
                pairsOfSocks = pairsOfSocks + count / 2;
            }
        }
        return pairsOfSocks;
    }

    public static void main (String[]args){

            int[] ar = new int[]{10, 20, 20, 10, 10, 30, 50, 10, 20};
//        int[] ar = new int[] {1, 2, 1, 2, 1, 3, 2};
        int n = ar.length;
        int numberOfPairs = sockMerchant(ar, n);
        System.out.println("Standard approach:\nNumber of pairs: " + numberOfPairs);
    }
}