package solvingproblems.easy.tenpoints.gradinstudents;

import java.util.ArrayList;
import java.util.List;

public class Solution {

    public static List<Integer> gradingStudents(List<Integer> grades) {
        int findNearestMultipleOf5;

        for (int i = 0; i < grades.size(); i++) {
            int studentGrade = grades.get(i);
            findNearestMultipleOf5 = (studentGrade / 5 + 1) * 5;

            if (studentGrade <= 37) {
                studentGrade += 0;
            }
             else if (findNearestMultipleOf5 - studentGrade < 3) {
                studentGrade = findNearestMultipleOf5;
            }
             else if (findNearestMultipleOf5 - studentGrade > 3) {
                studentGrade += 0;
            }
            grades.set(i, studentGrade);
        }
        return grades;
    }


    public static void main(String[] args) {
        
        List<Integer> grades = new ArrayList<>(
                List.of(64, 24, 68, 14, 53, 49, 45, 99, 55, 24, 59, 67, 8, 76, 37, 24, 24, 73, 81));
        gradingStudents(grades);
        System.out.println(grades);
    }
}
