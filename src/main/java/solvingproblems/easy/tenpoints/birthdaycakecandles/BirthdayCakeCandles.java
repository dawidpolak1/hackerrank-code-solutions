package solvingproblems.easy.tenpoints.birthdaycakecandles;

import java.util.Collections;
import java.util.List;

class BirthdayCakeCandles {

    public static int birthdayCakeCandles(List<Integer> candles) {
        int maxNumber = Collections.max(candles);
        int count = 0;
        for (Integer candle : candles) {
            if (candle == maxNumber) {
                count++;
            }
        }
        return count;
    }

}

class Solution {
    public static void main(String[] args) {

        List<Integer> candles = List.of(7, 1, 4, 3, 7, 2, 3, 7);
        int maxElements = BirthdayCakeCandles.birthdayCakeCandles(candles);
        System.out.println(maxElements);
    }
}

