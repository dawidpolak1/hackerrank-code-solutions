package objectorientedprogramming.javaabstractclass;

import java.util.Scanner;

public class Solution {

static abstract class Book {

    String title;
    abstract void setTitle(String s);
    String getTitle(){
        return title;
    }
}

static class MyBook extends Book {

    @Override
    void setTitle(String s) {
        System.out.println("The title is: " + s);
    }
}

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        MyBook myBook = new MyBook();
        myBook.setTitle(s);
    }
}
