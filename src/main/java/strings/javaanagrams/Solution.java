package strings.javaanagrams;

import java.util.HashMap;
import java.util.Map;

public class Solution {

    private static boolean isAnagram(String a, String b) {
        java.util.Map<Character, Integer> charsA = new java.util.HashMap<>();
        java.util.Map<Character, Integer> charsB = new java.util.HashMap<>();
        String first = a.toLowerCase();
        String second = b.toLowerCase();

        for (int i = 0; i < a.length(); i++) {
            if (charsA.containsKey(first.charAt(i)))
                charsA.put(first.charAt(i), charsA.get(first.charAt(i)) + 1);
            else
                charsA.put(first.charAt(i), 1);
        }

        for (int j = 0; j < b.length(); j++) {
            if (charsB.containsKey(second.charAt(j)))
                charsB.put(second.charAt(j), charsB.get(second.charAt(j)) + 1);
            else
                charsB.put(second.charAt(j), 1);
        }

//        if (charsA.equals(charsB)) {
//            System.out.println("Anagrams");
//        } else
//            System.out.println("Not Anagrams");
        return charsA.equals(charsB);
    }

    public static void prCharWithFreq(String s)
    {

        // Store all characters and
        // their frequencies in dictionary
        Map<Character, Integer> d = new HashMap<Character, Integer>();


        for(int i = 0; i < s.length(); i++)
        {
            if(d.containsKey(s.charAt(i)))
            {
                d.put(s.charAt(i), d.get(s.charAt(i)) + 1);
            }
            else
            {
                d.put(s.charAt(i), 1);
            }
        }

        // Print characters and their
        // frequencies in same order
        // of their appearance
        for(int i = 0; i < s.length(); i++)
        {

            // Print only if this
            // character is not printed
            // before
            if(d.get(s.charAt(i)) != 0)
            {
                System.out.print(s.charAt(i) + "-");
                System.out.print(d.get(s.charAt(i)) + " ");
                d.put(s.charAt(i), 0);
            }
        }
    }

    public static void main(String[] args) {

        String a = "anagramm";
        String b = "marganaa";

        isAnagram(a, b);
    }
}
