package datastructures.java1darraypart2;

public class SolutionWithRecursion {

    static boolean canWin(int[] game, int jump, int index) {
            if (index >= game.length) {
                return true;
            } else if (index < 0 || game[index] == 1) {
                return false;
            }

            game[index] = 1; // marks as visited

            // Recursive calls
            return  canWin(game, jump, index + jump) ||
                    canWin(game, jump, index + 1) ||
                    canWin(game, jump, index - 1);
    }


    public static void main(String[] args) {

        int[] game = {0, 0, 0, 1, 1, 1};
        int jump = 5;

        String result = canWin(game, jump, game[0]) ? "YES" : "NO";
        System.out.println(result);
    }
}

