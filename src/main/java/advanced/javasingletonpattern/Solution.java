package advanced.javasingletonpattern;

import java.util.Scanner;

public class Solution {

    static class Singleton {

        private static Singleton instance = null;
        //public String string;

        private Singleton() {
            if (instance != null) {
                throw new RuntimeException("Permission denied. Please use getInstance() method");
            }
        }

        public static Singleton getInstance() {
            if (instance == null) {
                instance = new Singleton();
            }
            return instance;
        }
    }

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        String string = in.nextLine();

        System.out.println("Hello I am a " + Singleton.getInstance().getClass().getSimpleName().toLowerCase() + "!" +
                " Let me say " + string + " to you");
    }
}
