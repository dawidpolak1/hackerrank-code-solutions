package strings.javastringreverse;

public class Solution {

    private static final String WORD = "madam";

    private static void checkIfWordIsPalindrome(String word) {
        StringBuilder sb = new StringBuilder();
        String reversed = sb.append(word).reverse().toString();

        String result = reversed.equals(word) ? "YES" : "NO";
        System.out.println(result);
    }

    public static void main(String[] args) {
        checkIfWordIsPalindrome(WORD);

    }
}
