package solvingproblems.easy.twentypoints.designerpdfviewer;

import java.util.HashMap;
import java.util.Map;

public class Solution {

    static int designerPdfViewer(int[] h, String word) {

        Map<Character, Integer> heightOfLetters = getCharacterWithHeight(h);

        return getSizeOfWordArea(word, heightOfLetters);
    }

    private static int getSizeOfWordArea(String word, Map<Character, Integer> heightOfLetters) {
        int length = word.length();
        int maxHeight = 0;
        for (int i = 0; i < length; i++) {
            int heightOfChar = heightOfLetters.get(word.charAt(i));
            if (heightOfChar > maxHeight)
                maxHeight = heightOfChar;
        }

        return maxHeight * length;
    }

    private static Map<Character, Integer> getCharacterWithHeight(int[] h) {
        Map<Character, Integer> heightOfLetters = new HashMap<>();
        char a;
        int index = 0;

        for (a = 'a'; a <= 'z'; a++) {
            heightOfLetters.put(a, h[index]);
            index++;
        }
        return heightOfLetters;
    }

    public static void main(String[] args) {

        int[] h = new int[] {
                1, 3, 1, 3, 1, 4, 1, 3, 2, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 7};

        String word = "zaba";

        int result = designerPdfViewer(h, word);
        System.out.println(result);


    }
}
