package bignumber.javabigdecimal;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;

public class Solution {

    private static void reversedOrder(BigDecimal[] bdFromStrings) {
        for (int i = 0; i < bdFromStrings.length; i++) {
            for (int j = i + 1; j < bdFromStrings.length; j++) {
                if (bdFromStrings[i].compareTo(bdFromStrings[j]) < 0) {
                    BigDecimal temp = bdFromStrings[i];
                    bdFromStrings[i] = bdFromStrings[j];
                    bdFromStrings[j] = temp;
                }
            }
        }
    }
    public static void main(String[] args) {

        /* Solution 1

        String[] strings = new String[] {"9", "-100",  "50", "0", "56.6", "90", "0.12", ".12", "02.34", "000.000"};
        BigDecimal[] bdFromStrings = new BigDecimal[strings.length];

        for (int i = 0; i < strings.length; i++) {
            bdFromStrings[i] = new BigDecimal(strings[i]);
            //System.out.println(bdFromStrings[i]);
        }

        reversedOrder(bdFromStrings);

        System.out.println(Arrays.toString(bdFromStrings));

         */


        /*
        Solution 2 fixed leading zeroes in string
         */
        String[] strings = new String[] {"9", "-100",  "50", "0", "56.6", "90", "0.12", ".12", "02.34", "000.000"};
        Arrays.sort(strings, Collections.reverseOrder((string1, string2) -> {
            BigDecimal previous = new BigDecimal(string1);
            BigDecimal next = new BigDecimal(string2);
            return previous.compareTo(next);
        }));

        System.out.println(Arrays.toString(strings));
      }
}
