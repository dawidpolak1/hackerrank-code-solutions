package strings.javasubstringcomparisions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Solution {

    public static String getSmallestAndLargest(String s, int k) {
        String smallest = "";
        String largest = "";

        List<String> words = new ArrayList<>();
        for (int i = 0; i < s.length() - k + 1; i++) {
            words.add(s.substring(i, i + k));
        }
        Collections.sort(words);
        smallest = words.get(0);
        largest = words.get(words.size() - 1);
        return smallest + "\n" + largest;
    }

    public static void main(String[] args) {

        String s = "welcometojava";
        int k = 3;
        String smallestAndLargest = getSmallestAndLargest(s, k);
        System.out.println(smallestAndLargest);
    }
}