package strings.validusernameregularexpression;

public class Solution {

    private static final String regex = "[a-zA-Z]{1}\\w{7,29}";

    public static void main(String[] args) {

        String[] userNames = {"Julia", "Samantha", "Samantha_21", "1Samantha",
                            "Samantha?10_2A", "JuliaZ007", "Julia@007", "_Julia007"};

        int numOfUserNames = userNames.length;
        for (int i = 0; i < numOfUserNames; i++) {
            if (userNames[i].matches(regex))
                System.out.println("Valid");
            else
                System.out.println("Invalid");
        }
    }
}
