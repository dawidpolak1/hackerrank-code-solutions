package datastructures.javalist;

import java.util.ArrayList;
import java.util.List;

public class Solution {

    public static void main(String[] args) {

        // Integer, the initial number of elements in List
        int elements = 5;

        // Integers describing this list
        List<Integer> numbers = new ArrayList<>(elements);
        numbers.add(12);
        numbers.add(0);
        numbers.add(1);
        numbers.add(78);
        numbers.add(12);

        // Number of queries
        String[] queries = new String[] {"Insert", "Delete"};
        int numberOfQueries = queries.length;

        for (String query : queries) {
            if (query.equals("Insert")) {
                numbers.add(5, 23);
            } else {
                numbers.remove(0);
            }
        }
        System.out.println(numbers);




    }
}
