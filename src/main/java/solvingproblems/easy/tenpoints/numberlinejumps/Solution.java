package solvingproblems.easy.tenpoints.numberlinejumps;

public class Solution {

    static String kangaroo(int startPoint1, int metersPerJump1, int startPoint2, int metersPerJump2) {
        int startPositionDifference = startPoint1 - startPoint2;
        int metersPerJumpDifference = metersPerJump2 - metersPerJump1;
        return meetAtTheSameLocation(startPositionDifference, metersPerJumpDifference) ? "YES" : "NO";
    }

    private static boolean meetAtTheSameLocation(int startPositionDifference, int metersPerJumpDifference) {
        return startPositionDifference % metersPerJumpDifference == 0;
    }

    public static void main(String[] args) {
        int startPoint1 = 0;
        int metersPerJump1 = 2;
        int startPoint2 = 5;
        int metersPerJump2 = 3;

        String result = kangaroo(startPoint1, metersPerJump1, startPoint2, metersPerJump2);
        System.out.println(result);
    }
}
