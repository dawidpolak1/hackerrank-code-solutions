package bignumber.javabiginteger;

import java.math.BigInteger;

public class Solution {

    public static void main(String[] args) {

        BigInteger a = new BigInteger("1234");
        BigInteger b = new BigInteger("20");

        BigInteger result = a.multiply(b);
        System.out.println(result);
    }
}
