package objectorientedprogramming.javainterface;

import java.util.Scanner;

public class Solution {

    interface AdvancedArithmetic {

        int divisorSum(int n);
    }

    static class MyCalculator implements AdvancedArithmetic {

        @Override
        public int divisorSum(int n) {
            int sum = 0;
            int sqrt = (int) Math.sqrt(n);

            for (int i = 1; i <= sqrt; i++) {
            if (n % i == 0) { // if "i" is a divisor
                sum += i + n/i; // add both divisors
            }

                /* If sqrt is a divisor, we should only count it once */
                if (sqrt * sqrt == n) {
                    sum -= sqrt;
                }
        }
            return sum;
        }
    }

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        System.out.println("I implemented: " + AdvancedArithmetic.class.getSimpleName());
        MyCalculator myCalculator = new MyCalculator();
        System.out.println(myCalculator.divisorSum(n));
    }
}
