package strings.javastringsintroduction;

public class Solution {

    public static void main(String[] args) {

        String a = "hello";
        String b = "java";

        int lengthOfWords = a.length() + b.length();
        System.out.println(lengthOfWords);
       System.out.println(a.compareTo(b) > 0 ? "Yes" : "No");
        String aFirstLetter = a.substring(0, 1).toUpperCase();
        String bFirstLetter = b.substring(0, 1).toUpperCase();
        System.out.println(aFirstLetter + a.substring(1) + " " +  bFirstLetter + b.substring(1));
    }
}
