package introduction.javadateandtime;

import java.time.DayOfWeek;
import java.time.LocalDate;

class Result {

    public static String findDay(int month, int day, int year) {
        LocalDate date = LocalDate.of(year, month, day);
        DayOfWeek findDay = date.getDayOfWeek();
        return String.valueOf(findDay);
    }
}

public class Solution {

    public static void main(String[] args) {

        int month = 8;
        int day = 5;
        int year = 2015;

        String findDay = Result.findDay(month, day, year);
        System.out.println(findDay);
    }
}
