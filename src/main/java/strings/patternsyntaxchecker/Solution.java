package strings.patternsyntaxchecker;

import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class Solution {

    public static void main(String[] args) {

//        String patternValid = "([A-Z])(.+)";
//        String patternInvalid = "[AZ[a-z](a-z)";

        try (Scanner sc = new Scanner(System.in)) {
            for (int i = 0; i < 2; i++) {
                String pattern = sc.nextLine();
                Pattern.compile(pattern);
                System.out.println("Valid");
            }
        } catch (PatternSyntaxException e) {
            System.out.println("Invalid");
        }
    }
}
